from google.appengine.ext import ndb


class GuestBook(ndb.Model):
    full_name = ndb.StringProperty()
    email = ndb.StringProperty()
    message = ndb.TextProperty()
    date = ndb.DateTimeProperty(auto_now_add=True)
    deleted = ndb.BooleanProperty(default=False)

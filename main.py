#!/usr/bin/env python
import os
import jinja2
import webapp2
import time
from models import GuestBook

template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=False)


class BaseHandler(webapp2.RequestHandler):

    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        template = jinja_env.get_template(view_filename)
        self.response.out.write(template.render(params))


class MainHandler(BaseHandler):
    def get(self):
        return self.render_template("index.html")


class GuestbookHandler(BaseHandler):
    def get(self):

        entries = GuestBook.query(GuestBook.deleted == False).fetch()
        entries.sort(key=lambda r: r.date, reverse=True)

        params = {"entries": entries}

        return self.render_template("guestbook.html", params=params)

    def post(self):

        full_name = self.request.get("full_name")
        email = self.request.get("email")
        message = self.request.get("message")

        if not full_name:
            full_name = 'anonimous'

        if '<script>' not in full_name and '<script>' not in email and '<script>' not in message:
            guestbook_entry = GuestBook(full_name=full_name, email=email, message=message)
            guestbook_entry.put()
            time.sleep(0.1)
            entries = GuestBook.query(GuestBook.deleted == False).fetch()
            entries.sort(key=lambda r: r.date, reverse=True)

            params = {
                'entries': entries,
                'result': 'Thank you for submitting your message!'
            }
        else:

            entries = GuestBook.query(GuestBook.deleted == False).fetch()
            entries.sort(key=lambda r: r.date, reverse=True)

            params = {
                'entries': entries,
                'result': 'HAHA! You have been reported to the FBI for using scripts!!!'
            }

        return self.render_template("guestbook.html", params=params)


class MessageHandler(BaseHandler):
    def get(self, message_id):
        message = GuestBook.get_by_id(int(message_id))
        params = {"message": message}
        return self.render_template("message.html", params=params)


class EditMessageHandler(BaseHandler):
    def get(self, message_id):
        message = GuestBook.get_by_id(int(message_id))
        params = {"message": message}
        return self.render_template("edit_message.html", params=params)

    def post(self, message_id):
        message = self.request.get("edit_message")
        guestbook_entry = GuestBook.get_by_id(int(message_id))
        guestbook_entry.message = message
        guestbook_entry.put()
        time.sleep(0.1)
        return self.redirect_to("guestbook")


class DeleteMessageHandler(BaseHandler):
    def get(self, message_id):
        message = GuestBook.get_by_id(int(message_id))
        params = {"message": message}
        return self.render_template("delete_message.html", params=params)

    def post(self, message_id):
        message = GuestBook.get_by_id(int(message_id))
        message.deleted = True
        message.put()
        time.sleep(0.1)
        return self.redirect_to("guestbook")


class ViewDeletedHandler(BaseHandler):
    def get(self):

        entries = GuestBook.query(GuestBook.deleted == True).fetch()
        entries.sort(key=lambda r: r.date, reverse=True)

        if entries:
            params = {"entries": entries}
        else:
            params = {"message": "There are no deleted messages! Try adding a message in guestbook and delete it."}

        return self.render_template("deleted_messages.html", params=params)


class RestoreMessageHandler(BaseHandler):

    def get(self, message_id):
        message = GuestBook.get_by_id(int(message_id))
        params = {"message": message}
        return self.render_template("restore_message.html", params=params)

    def post(self, message_id):
        message = GuestBook.get_by_id(int(message_id))
        message.deleted = False
        message.put()
        time.sleep(0.1)
        return self.redirect_to("guestbook")


class PermDeleteMessageHandler(BaseHandler):

    def get(self, message_id):
        message = GuestBook.get_by_id(int(message_id))
        params = {"message": message}
        return self.render_template("delete_forever.html", params=params)

    def post(self, message_id):
        message = GuestBook.get_by_id(int(message_id))
        message.key.delete()
        time.sleep(0.1)

        entries = GuestBook.query(GuestBook.deleted == True).fetch()

        if entries:
            return self.redirect_to("deleted")
        else:
            return self.redirect_to("guestbook")


app = webapp2.WSGIApplication([
    webapp2.Route('/', MainHandler),
    webapp2.Route('/guestbook', GuestbookHandler, name="guestbook"),
    webapp2.Route('/message/<message_id:\d+>', MessageHandler),
    webapp2.Route('/message/<message_id:\d+>/edit', EditMessageHandler),
    webapp2.Route('/message/<message_id:\d+>/delete', DeleteMessageHandler),
    webapp2.Route('/deleted-messages', ViewDeletedHandler, name="deleted"),
    webapp2.Route('/message/<message_id:\d+>/restore', RestoreMessageHandler),
    webapp2.Route('/message/<message_id:\d+>/delete-forever', PermDeleteMessageHandler),
], debug=True)
